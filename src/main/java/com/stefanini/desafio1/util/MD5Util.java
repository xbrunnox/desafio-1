package com.stefanini.desafio1.util;

import java.security.MessageDigest;

/**
 * <b>MD5Util</b><br>
 * Classe utilitária para gerar MD5 a partir de uma String.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de julho de 2020.
 *
 */
public class MD5Util {

	/**
	 * Gera um hash MD5 para o texto indicado, retornando null caso o texto seja
	 * vazio ou se ocorra alguma Exceção durante o processo de conversão.
	 * 
	 * @param texto Texto.
	 * @return MD5 correspondente ao texto indicado.
	 */
	public static String toMd5(final String texto) {
		StringBuffer sb = new StringBuffer();
		try {
			final MessageDigest md = MessageDigest.getInstance("MD5");
			final byte[] array = md.digest(texto.getBytes("UTF-8"));
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
			}
			return sb.toString();
		} catch (Exception e) {
			// e.printStackTrace();
			return null;
		}
	}

}
