package com.stefanini.desafio1.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	
	/**
	 * Retorna se o endereço de e-mail é válido.
	 * @param email Endereço de e-mail.
	 * @return true/false.
	 */
	public static boolean isValidEmail(String email) {
	    boolean isEmailIdValid = false;
	    if (email != null && email.length() > 0) {
	        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
	        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
	        Matcher matcher = pattern.matcher(email);
	        if (matcher.matches()) {
	            isEmailIdValid = true;
	        }
	    }
	    return isEmailIdValid;
	}
	
	public static boolean isValidMinimunLength(String string, int minimumLength) {
		if (string == null)
			return false;
		if (string.length() >= minimumLength)
			return true;
		return false;
	}
	
	public static boolean isValidMaximumLength(String string, int maximo) {
		if (string == null)
			return false;
		if (string.length() <= maximo)
			return true;
		return false;
	}

}
