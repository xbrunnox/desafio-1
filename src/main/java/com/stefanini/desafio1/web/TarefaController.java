package com.stefanini.desafio1.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.tarefa.exception.TarefaException;
import com.stefanini.desafio1.tarefa.service.TarefaService;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>TarefaController</b><br>
 * Controlador WEB responsável pelas operações web com a entidade Tarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Controller
@RequestMapping("/tarefa")
public class TarefaController {

	@Autowired
	private TarefaService tarefaService;

	@Autowired
	private HttpServletRequest request;

	/**
	 * Realiza o tratamento de exceções relativas a entidade Tarefa.
	 * 
	 * @param tarefaException Exceção da entidade Tarefa.
	 * @return Mapa contendo os campos e a mensagem de erro.
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(TarefaException.class)
	public @ResponseBody Map<String, String> handleValidationExceptions(TarefaException tarefaException) {
		Map<String, String> errors = new HashMap<>();
		if (tarefaException.getErros() != null)
			for (String chave : tarefaException.getErros().keySet())
				errors.put(chave, tarefaException.getErros().get(chave));
		errors.put("mensagem", tarefaException.getMessage());
		return errors;
	}

	/**
	 * Adiciona uma tarefa na lista indicada.
	 * 
	 * @param idLista   ID da lista.
	 * @param descricao Descrição da tarefa.
	 * @return Json contendo o objeto que foi criado.
	 */
	@PostMapping("/adicionar")
	public @ResponseBody Tarefa adicionar(@RequestParam int idLista, @RequestParam String descricao) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		return tarefaService.gravar(usuario, idLista, descricao);
	}

	/**
	 * Retorna a tarefa com o ID indicado.
	 * 
	 * @param idTarefa ID da tarefa.
	 * @return Tarefa.
	 */
	@GetMapping("/get/{idTarefa}")
	public @ResponseBody Tarefa get(@PathVariable(value = "idTarefa") int idTarefa) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		return tarefaService.get(usuario, idTarefa);
	}

	/**
	 * Realiza a exclusão da tarefa com o ID indicado.
	 * 
	 * @param idTarefa ID da tarefa a ser removida.
	 * @return Retorna ok se a tarefa for excluída.
	 */
	@GetMapping("/excluir/{idTarefa}")
	public @ResponseBody String excluir(@PathVariable(value = "idTarefa") int idTarefa) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		tarefaService.excluir(usuario, idTarefa);
		return "ok";
	}
}
