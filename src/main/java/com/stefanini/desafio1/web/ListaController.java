package com.stefanini.desafio1.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.exception.ListaDeTarefaException;
import com.stefanini.desafio1.tarefa.service.ListaDeTarefaService;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>ListaController</b><br>
 * Controlador WEB responsável pelas operações HTTP da entidade ListaDeTarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 08 de agosto de 2020.
 */
@Controller
@RequestMapping("/lista")
public class ListaController {

	@Autowired
	private ListaDeTarefaService listaDeTarefasService;

	@Autowired
	private HttpServletRequest request;

	/**
	 * Realiza o tratamento de exceções relativas a entidade Tarefa.
	 * 
	 * @param listaDeTarefaException Exceção da entidade Tarefa.
	 * @return Mapa contendo os campos e a mensagem de erro.
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ListaDeTarefaException.class)
	public @ResponseBody Map<String, String> handleValidationExceptions(ListaDeTarefaException listaDeTarefaException) {
		Map<String, String> errors = new HashMap<>();
		if (listaDeTarefaException.getErros() != null)
			for (String chave : listaDeTarefaException.getErros().keySet())
				errors.put(chave, listaDeTarefaException.getErros().get(chave));
		errors.put("mensagem", listaDeTarefaException.getMessage());
		return errors;
	}

	/**
	 * Adiciona uma nova lista de tarefas.
	 * 
	 * @param nome Nome da lista.
	 * @return Lista após a criação.
	 */
	@PostMapping("/adicionar")
	public @ResponseBody ListaDeTarefa adicionar(@RequestParam String nome) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		ListaDeTarefa lista = listaDeTarefasService.gravar(usuario, nome);
		return lista;
	}

	/**
	 * Retorna a lista de tarefas preenchidas com as respectivas tarefas.
	 * 
	 * @return Lista de tarefas.
	 */
	@GetMapping("/listasDeTarefas")
	public @ResponseBody List<ListaDeTarefa> listasDeTarefas() {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		return listaDeTarefasService.getListComTarefas(usuario);
	}

	/**
	 * Remove a lista com o ID indicado.
	 * 
	 * @param idLista ID da lista.
	 * @return ok, caso a operação seja bem sucedida.
	 */
	@GetMapping("/excluir/{idLista}")
	public @ResponseBody String excluir(@PathVariable(value = "idLista") int idLista) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		listaDeTarefasService.excluir(usuario, idLista);
		return "ok";
	}

	/**
	 * Retorna a lista de tarefas com o id indicado.
	 * 
	 * @param idLista ID da lista de tarefas.
	 * @return Lista de tarefas.
	 */
	@GetMapping("/get/{idLista}")
	public @ResponseBody ListaDeTarefa get(@PathVariable(value = "idLista") int idLista) {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		return listaDeTarefasService.get(usuario, idLista);
	}

}
