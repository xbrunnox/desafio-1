package com.stefanini.desafio1.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.stefanini.desafio1.usuario.Usuario;
import com.stefanini.desafio1.usuario.exception.UsuarioException;
import com.stefanini.desafio1.usuario.service.UsuarioService;

/**
 * <b>UsuarioController</b><br>
 * Controlador Web responsável pela manipulação de dados da entidade Usuario.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private HttpServletRequest request;

	/**
	 * Realiza o tratamento de exceções relativas a entidade Usuario.
	 * 
	 * @param usuarioException Exceção de usuário.
	 * @return Mapa contendo os campos e a mensagem de erro.
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(UsuarioException.class)
	public @ResponseBody Map<String, String> handleValidationExceptions(UsuarioException usuarioException) {
		Map<String, String> errors = new HashMap<>();
		if (usuarioException.getErros() != null)
			for (String chave : usuarioException.getErros().keySet())
				errors.put(chave, usuarioException.getErros().get(chave));
		errors.put("mensagem", usuarioException.getMessage());
		return errors;
	}

	/**
	 * Realiza a autenticação do usuário no sistema.
	 * 
	 * @param email E-mail.
	 * @param senha Senha.
	 * @return Resultado da requisição.
	 */
	@PostMapping("/autenticar")
	public @ResponseBody String autenticar(@RequestParam String email, @RequestParam String senha) {
		Usuario usuario = usuarioService.autenticar(email, senha);
		request.getSession().setAttribute("usuario", usuario);
		return "ok";
	}

	/**
	 * Exibe a VIEW para cadastro de um novo usuário.
	 * 
	 * @return View para cadastro.
	 */
	@GetMapping("/novo")
	public String novo() {
		return "usuario/novo";
	}

	/**
	 * Realiza o registro do usuário.
	 * 
	 * @param nome  Nome do usuário.
	 * @param email E-mail.
	 * @param senha Senha.
	 * @return Retorno da requisição.
	 */
	@PostMapping("/registrar")
	public @ResponseBody String registrar(@RequestParam String nome, String email, String senha) {
		usuarioService.registrar(nome, email, senha);
		return "ok";
	}

	/**
	 * Realiza o encerramento da conta do usuário logado.
	 * 
	 * @return ok, se a operação for bem sucedida.
	 */
	@GetMapping("/encerrarConta")
	public @ResponseBody String encerrarConta() {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		usuarioService.encerrarConta(usuario);
		request.getSession().setAttribute("usuario", null);
		return "ok";
	}

}
