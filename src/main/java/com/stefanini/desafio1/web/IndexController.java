package com.stefanini.desafio1.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stefanini.desafio1.tarefa.service.ListaDeTarefaService;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>IndexController</b><br>
 * Controlador web responsável pelas chamadas HTTP da raiz (/).
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Controller
@RequestMapping("/")
public class IndexController {

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private ListaDeTarefaService listaService;

	/**
	 * Exibe a VIEW padrão, do index.
	 * 
	 * @return Endereço da view de Index, ou redirect para login caso o usuário não
	 *         esteja logado.
	 */
	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView model = new ModelAndView("index/index");
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		if (usuario == null)
			return new ModelAndView("redirect:/login");
		model.addObject("usuario", usuario);
		return model;
	}

	/**
	 * Exibe a VIEW para realização do Login.
	 * 
	 * @return VIEW para login.
	 */
	@RequestMapping("/login")
	public String login() {
		return "index/login";
	}

	/**
	 * Realiza o logout do sistema.
	 * 
	 * @return Redirect ao logout.
	 */
	@RequestMapping("/logout")
	public String logout() {
		request.getSession().setAttribute("usuario", null);
		return "redirect:/login";
	}

	/**
	 * Realiza o reset da aplicação, removendo todas as tarefas e todas as listas de
	 * tarefa do usuário logado.
	 * 
	 * @return Redirecionamento para o index.
	 */
	@RequestMapping("/reset")
	public String reset() {
		Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
		if (usuario == null)
			return "redirect:/login";
		listaService.reset(usuario);
		return "redirect:/";
	}

}
