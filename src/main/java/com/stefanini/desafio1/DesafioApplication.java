package com.stefanini.desafio1;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DesafioApplication {
	
	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(DesafioApplication.class);
        builder.headless(false);
        ConfigurableApplicationContext context = builder.run(args);
	}

}
