package com.stefanini.desafio1.tarefa;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <b>Tarefa</b><br>
 * Entidade que representa uma tarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Entity
@NamedQuery(name = "Tarefa.getList", query = "SELECT t FROM Tarefa t WHERE t.listaDeTarefa = ?1")
@NamedQuery(name = "Tarefa.getByUsuarioIdTarefa", query = "SELECT t FROM Tarefa t WHERE t.listaDeTarefa.usuario = ?1 AND t.id = ?2")
public class Tarefa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@JsonIgnore
	@ManyToOne
	private ListaDeTarefa listaDeTarefa;

	private Timestamp dataDeCadastro;

	@Lob 
	@Column(length=200)
	private String descricao;

	/**
	 * Retorna o ID da tarefa.
	 * 
	 * @return ID da tarefa.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Define o ID da tarefa.
	 * 
	 * @param id ID da tarefa.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retorna a data de cadastro.
	 * 
	 * @return Data de cadastro.
	 */
	public Timestamp getDataDeCadastro() {
		return dataDeCadastro;
	}

	/**
	 * Define a data de cadastro.
	 * 
	 * @param dataDeCadastro Data de cadastro.
	 */
	public void setDataDeCadastro(Timestamp dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	/**
	 * Retorna a descrição da tarefa.
	 * 
	 * @return Descrição da tarefa.
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Define a descrição da tarefa.
	 * 
	 * @param descricao Descrição da tarefa.
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Retorna a lista de tarefa à qual pertence a tarefa.
	 * 
	 * @return Lista de tarefa.
	 */
	public ListaDeTarefa getListaDeTarefa() {
		return listaDeTarefa;
	}

	/**
	 * Define a lista de tarefa à qual pertence a tarefa.
	 * 
	 * @param listaDeTarefa Lista de tarefa.
	 */
	public void setListaDeTarefa(ListaDeTarefa listaDeTarefa) {
		this.listaDeTarefa = listaDeTarefa;
	}
}