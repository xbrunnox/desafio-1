package com.stefanini.desafio1.tarefa.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.tarefa.database.ListaDeTarefaDB;
import com.stefanini.desafio1.tarefa.database.TarefaDB;
import com.stefanini.desafio1.tarefa.exception.ListaDeTarefaException;
import com.stefanini.desafio1.tarefa.util.ListaDeTarefaValidator;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>ListaDeTarefaService</b><br>
 * Implementação de Serviços para a entidade ListaDeTarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Service
public class ListaDeTarefaService {

	@Autowired
	private ListaDeTarefaDB listaDatabase;

	@Autowired
	private TarefaDB tarefaDatabase;

	/**
	 * Retorna uma lista contendo as listas de tarefas pertencentes ao usuário
	 * indicado.
	 * 
	 * @param usuario Usuário.
	 * @return Lista com ListaDeTarefa.
	 */
	public List<ListaDeTarefa> getList(Usuario usuario) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível carregar lista de tarefas, o usuário não está logado.");
		return listaDatabase.getList(usuario);
	}

	/**
	 * Retorna uma lista contendo as listas de tarefas pertencentes ao usuário
	 * indicado, preenchidas com as respectivas tarefas.
	 * 
	 * @param usuario Usuário.
	 * @return Lista com ListaDeTarefa.
	 */
	public List<ListaDeTarefa> getListComTarefas(Usuario usuario) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível carregar lista de tarefas, o usuário não está logado.");
		List<ListaDeTarefa> retorno = listaDatabase.getList(usuario);
		for (ListaDeTarefa lista : retorno)
			lista.setTarefas(tarefaDatabase.getList(lista));
		return retorno;
	}

	/**
	 * Efetua a gravação da lista de tarefas.
	 * 
	 * @param usuario Usuário.
	 * @param nome    Nome da lista.
	 * @return Lista após a gravação.
	 */
	public ListaDeTarefa gravar(Usuario usuario, String nome) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível gravar a lista, o usuário não está logado.");
		if (nome != null)
			nome = nome.trim();
		ListaDeTarefa lista = new ListaDeTarefa();
		lista.setUsuario(usuario);
		lista.setDataDeCadastro(new Timestamp(System.currentTimeMillis()));
		lista.setNome(nome);
		// Realiza a validação da lista, lançando uma ListaDeTarefaException caso algum
		// parâmetro falhe a validação.
		ListaDeTarefaValidator.validate(lista, "Não foi possível gravar lista.");
		listaDatabase.save(lista);
		return lista;
	}

	/**
	 * Realiza a exclusão da lista com o ID indicado.
	 * 
	 * @param usuario Usuário proprietário da lista.
	 * @param idLista ID da lista.
	 */
	@Transactional
	public void excluir(Usuario usuario, int idLista) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível excluir lista, o usuário não está logado.");
		ListaDeTarefa lista = listaDatabase.getByUsuarioIdLista(usuario, idLista);
		if (lista != null) {
			List<Tarefa> tarefas = tarefaDatabase.getList(lista);
			for (Tarefa tarefa : tarefas)
				tarefaDatabase.delete(tarefa);
			listaDatabase.delete(lista);
		} else {
			throw new ListaDeTarefaException("Não foi possível excluir, a lista não existe.");
		}
	}

	/**
	 * Realiza o reset do usuário indicado, removendo todas as listas e todas as
	 * tarefas.
	 * 
	 * @param usuario Usuário.
	 */
	@Transactional
	public void reset(Usuario usuario) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível realizar RESET, o usuário não está logado.");
		List<ListaDeTarefa> retorno = listaDatabase.getList(usuario);
		for (ListaDeTarefa lista : retorno) {
			List<Tarefa> tarefas = tarefaDatabase.getList(lista);
			for (Tarefa tarefa : tarefas)
				tarefaDatabase.delete(tarefa);
			listaDatabase.delete(lista);
		}
	}

	/**
	 * Retorna a lista com o ID indicado e pertencente ao usuário.
	 * 
	 * @param usuario Usuário.
	 * @param idLista ID da lista.
	 * @return Lista de tarefas.
	 */
	public ListaDeTarefa get(Usuario usuario, int idLista) {
		if (usuario == null)
			throw new ListaDeTarefaException("Não foi possível carregar a lista, o usuário não está logado.");
		return listaDatabase.getByUsuarioIdLista(usuario, idLista);
	}

}
