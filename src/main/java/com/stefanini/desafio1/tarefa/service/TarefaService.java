package com.stefanini.desafio1.tarefa.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.tarefa.database.ListaDeTarefaDB;
import com.stefanini.desafio1.tarefa.database.TarefaDB;
import com.stefanini.desafio1.tarefa.exception.TarefaException;
import com.stefanini.desafio1.tarefa.util.TarefaValidator;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>TarefaService</b><br>
 * Implementação de Service responsável pela entidade Tarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Service
public class TarefaService {

	@Autowired
	private TarefaDB tarefaDatabase;
	
	@Autowired
	private ListaDeTarefaDB listaDeTarefaDatabase;

	/**
	 * Realiza a exclusão da tarefa com o usuário e id informados.
	 * 
	 * @param usuario  Usuário
	 * @param idTarefa ID da tarefa
	 * @throws TarefaException Exceção da entidade Tarefa.
	 */
	public void excluir(Usuario usuario, int idTarefa) throws TarefaException {
		if (usuario == null)
			throw new TarefaException("Não foi possível excluir tarefa, o usuário não está logado.");
		Tarefa tarefa = tarefaDatabase.getByUsuarioIdTarefa(usuario, idTarefa);
		if (tarefa == null)
			throw new TarefaException("A tarefa não existe, ou já se encontra excluída.");
		tarefaDatabase.delete(tarefa);
	}

	/**
	 * Realiza a gravação da tarefa.
	 * 
	 * @param usuario   Usuário da tarefa.
	 * @param idLista   ID da lista, esta deve pertencer ao usuário.
	 * @param descricao Descrição da tarefa.
	 * @return Tarefa após a gravação.
	 * @throws TarefaException Exceção da entidade Tarefa.
	 */
	public Tarefa gravar(Usuario usuario, int idLista, String descricao) throws TarefaException {
		if (usuario == null)
			throw new TarefaException("Não foi possível gravar, o usuário não está logado.");
		ListaDeTarefa lista = listaDeTarefaDatabase.getByUsuarioIdLista(usuario, idLista);
		if (lista == null) 
			throw new TarefaException("Não foi possível gravar, a lista não existe.");
		Tarefa tarefa = new Tarefa();
		tarefa.setDataDeCadastro(new Timestamp(System.currentTimeMillis()));
		tarefa.setDescricao(descricao);
		tarefa.setListaDeTarefa(lista);
		TarefaValidator.validate(tarefa, "Não foi possível gravar tarefa.");
		tarefaDatabase.save(tarefa);
		return tarefa;
	}

	/**
	 * Retorna a tarefa com o ID indicado e pertencente ao Usuário indicado.
	 * 
	 * @param usuario  Usuário.
	 * @param idTarefa ID da tarefa.
	 * @return Tarefa.
	 */
	public Tarefa get(Usuario usuario, int idTarefa) {
		if (usuario == null)
			throw new TarefaException("Não foi possível recuperar tarefa, o usuário não está logado.");
		return tarefaDatabase.getByUsuarioIdTarefa(usuario, idTarefa);
	}

}