package com.stefanini.desafio1.tarefa.util;

import java.util.HashMap;
import java.util.Map;

import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.tarefa.exception.TarefaException;
import com.stefanini.desafio1.util.Validator;

/**
 * <b>TarefaValidator</b><br>
 * Validador para a entidade Tarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 08 de agosto de 2020.
 */
public class TarefaValidator extends Validator {

	/**
	 * Realiza a validação da tarefa, disparando uma TarefaException caso algum item
	 * não passe na validação.
	 * 
	 * @param tarefa         Tarefa.
	 * @param mensagemDeErro Mensagem de erro.
	 */
	public static void validate(Tarefa tarefa, String mensagemDeErro) {
		Map<String, String> erros = new HashMap<String, String>();
		// Descrição
		if (tarefa.getDescricao() == null || tarefa.getDescricao().isEmpty())
			erros.put("descricao", "A descrição da tarefa não pode ser vazia.");
		else if (!isValidMaximumLength(tarefa.getDescricao(), 200))
			erros.put("descricao", "O tamanho máximo da descrição da tarefa é 200 caracteres.");
		// Lista de tarefa
		if (tarefa.getListaDeTarefa() == null)
			erros.put("listaDeTarefa", "A tarefa deve pertencer à uma lista.");
		// Dispara exceção se existirem erros.
		if (!erros.isEmpty())
			throw new TarefaException(mensagemDeErro, erros);
	}

}
