package com.stefanini.desafio1.tarefa.util;

import java.util.HashMap;
import java.util.Map;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.exception.ListaDeTarefaException;
import com.stefanini.desafio1.util.Validator;

/**
 * <b>ListaDeTarefaValidator</b><br>
 * Validador para a entidade ListaDeTarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 08 de agosto de 2020.
 */
public class ListaDeTarefaValidator extends Validator {

	/**
	 * Realiza a validação da tarefa, disparando uma ListaDeTarefaException caso
	 * algum item não passe na validação.
	 * 
	 * @param listaDeTarefa  Tarefa.
	 * @param mensagemDeErro Mensagem de erro.
	 */
	public static void validate(ListaDeTarefa listaDeTarefa, String mensagemDeErro) {
		Map<String, String> erros = new HashMap<String, String>();
		// Nome
		if (listaDeTarefa.getNome() == null || listaDeTarefa.getNome().isEmpty())
			erros.put("nome", "O nome da lista não pode ser vazio.");
		else if (!isValidMaximumLength(listaDeTarefa.getNome(), 50))
			erros.put("nome", "O nome da lista não pode exceder 50 caracteres.");
		if (!erros.isEmpty())
			throw new ListaDeTarefaException(mensagemDeErro, erros);
	}

}
