package com.stefanini.desafio1.tarefa.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>ListaDeTarefaException</b><br>
 * Exceção relativa a entidade ListaDeTarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 08 de agosto de 2020.
 */
public class ListaDeTarefaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public String mensagem;

	private Map<String, String> erros;

	/**
	 * Construtor.
	 * 
	 * @param mensagem Mensagem do erro.
	 */
	public ListaDeTarefaException(String mensagem) {
		super(mensagem);
		erros = new HashMap<String, String>();
	}

	/**
	 * Construtor.
	 * 
	 * @param excecao Mensagem da exceção.
	 * @param erros   Mapa Contendo os erros.
	 */
	public ListaDeTarefaException(String excecao, Map<String, String> erros) {
		super(excecao);
		this.erros = erros;
		if (this.erros == null)
			erros = new HashMap<String, String>();
	}

	/**
	 * Adiciona o erro ao campo.
	 * 
	 * @param campo     Nome do campo.
	 * @param descricao Descrição do erro.
	 */
	public void addErro(String campo, String descricao) {
		erros.put(campo, descricao);
	}

	/**
	 * Retorna a mensagem de erro.
	 * 
	 * @return Mensagem de erro.
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Define a mensagem de erro.
	 * 
	 * @param mensagem Mensagem de erro.
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Retorna um mapa contendo os erros.
	 * 
	 * @return Mapa contendo os erros.
	 */
	public Map<String, String> getErros() {
		return erros;
	}

	/**
	 * Define um mapa contendo os erros.
	 * 
	 * @param erros Mapa contendo os erros.
	 */
	public void setErros(Map<String, String> erros) {
		this.erros = erros;
	}

}
