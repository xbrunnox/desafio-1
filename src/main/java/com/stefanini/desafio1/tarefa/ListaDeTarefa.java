package com.stefanini.desafio1.tarefa;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>ListaDeTarefa</b><br>
 * Entidade que representa uma lista de tarefas.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Entity
@NamedQuery(name = "ListaDeTarefa.getList", query = "SELECT ldf FROM ListaDeTarefa ldf WHERE ldf.usuario = ?1")
@NamedQuery(name = "ListaDeTarefa.getByUsuarioIdLista", query = "SELECT ldf FROM ListaDeTarefa ldf WHERE ldf.usuario = ?1 AND ldf.id = ?2")
public class ListaDeTarefa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@JsonIgnore
	@ManyToOne
	private Usuario usuario;

	private Timestamp dataDeCadastro;

	@Column(length = 50)
	private String nome;

	@Transient
	private List<Tarefa> tarefas;

	/**
	 * Retorna o ID da lista de tarefas.
	 * 
	 * @return ID da lista de tarefas.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Define o ID da lista de tarefas.
	 * 
	 * @param id ID da lista de tarefas.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retorna a data de cadastro.
	 * 
	 * @return Data de cadastro.
	 */
	public Timestamp getDataDeCadastro() {
		return dataDeCadastro;
	}

	/**
	 * Define a data de cadastro.
	 * 
	 * @param dataDeCadastro Data de cadastro.
	 */
	public void setDataDeCadastro(Timestamp dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	/**
	 * Retorna o nome da lista de tarefas.
	 * 
	 * @return Nome.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Define o nome da lista de tarefas.
	 * 
	 * @param nome Nome.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna o usuário ao qual a lista de tarefas pertence.
	 * 
	 * @return Usuário.
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * Define o usuário ao qual a lista de tarefas pertence.
	 * 
	 * @param usuario Usuário.
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * Retorna a lista de tarefas.
	 * 
	 * @return Lista de tarefas.
	 */
	public List<Tarefa> getTarefas() {
		return tarefas;
	}

	/**
	 * Define a lista de tarefas.
	 * 
	 * @param tarefas Lista de tarefas.
	 */
	public void setTarefas(List<Tarefa> tarefas) {
		this.tarefas = tarefas;
	}
}
