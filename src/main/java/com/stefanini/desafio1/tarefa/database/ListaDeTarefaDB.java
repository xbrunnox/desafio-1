package com.stefanini.desafio1.tarefa.database;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>ListaDeTarefaDB</b><br>
 * DAO responsável pelas operações de banco de dados da entidade ListaDeTarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
public interface ListaDeTarefaDB extends CrudRepository<ListaDeTarefa, Integer> {
	
	/**
	 * Retorna uma Lista com as Listas de Tarefas do Usuário indicado.
	 * @param usuario Usuario.
	 * @return Lista com as Listas de Tarefas.
	 */
	public List<ListaDeTarefa> getList(Usuario usuario);
	
	/**
	 * Retorna a lista de tarefa com o ID e usuário indicado.
	 * @param usuario Usuário.
	 * @param idLista ID da lista.
	 * @return Lista de Tarefa.
	 */
	public ListaDeTarefa getByUsuarioIdLista(Usuario usuario, int idLista);

}
