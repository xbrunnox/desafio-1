package com.stefanini.desafio1.tarefa.database;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.usuario.Usuario;

/**
 * <b>TarefaDB</b><br>
 * DAO responsável pela entidade Tarefa.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
public interface TarefaDB extends CrudRepository<Tarefa, Integer> {

	/**
	 * Retorna a lista de tarefas da lista indicada.
	 * @param lista Lista de Tarefas.
	 * @return Lista com as tarefas.
	 */
	public List<Tarefa> getList(ListaDeTarefa lista);
	
	/**
	 * Retorna a tarefa com o Usuario e ID informados
	 * @param usuario Usuário
	 * @param idTarefa ID da tarefa.
	 * @return Tarefa.
	 */
	public Tarefa getByUsuarioIdTarefa(Usuario usuario, int idTarefa);

}
