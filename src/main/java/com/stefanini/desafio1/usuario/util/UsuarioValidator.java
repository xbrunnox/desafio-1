package com.stefanini.desafio1.usuario.util;

import java.util.HashMap;
import java.util.Map;

import com.stefanini.desafio1.usuario.Usuario;
import com.stefanini.desafio1.usuario.exception.UsuarioException;
import com.stefanini.desafio1.util.Validator;

/**
 * <b>UsuarioValidator</b><br>
 * Validador para a entidade Usuario.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 08 de agosto de 2020.
 */
public class UsuarioValidator extends Validator {

	/**
	 * Realiza a validação do usuario, disparando um UsuarioException caso algum
	 * item não passe na validação.
	 * 
	 * @param usuario        Usuario.
	 * @param mensagemDeErro Mensagem de erro.
	 */
	public static void validate(Usuario usuario, String mensagemDeErro) {
		Map<String, String> erros = new HashMap<String, String>();
		// Nome
		if (usuario.getNome() == null || usuario.getNome().isEmpty())
			erros.put("nome", "O nome do usuario não pode ser vazio.");
		else if (!isValidMinimunLength(usuario.getNome(), 10))
			erros.put("nome", "O nome do usuario deve ter mais de 10 caracteres.");
		else if (!isValidMaximumLength(usuario.getNome(), 80))
			erros.put("nome", "O nome do usuario não deve ter mais que 80 caracteres.");
		// E-mail
		if (!isValidEmail(usuario.getEmail())) {
			erros.put("email", "Endereço de e-mail inválido.");
		} else if (!isValidMaximumLength(usuario.getEmail(), 60)) {
			erros.put("email", "Endereço de e-mail não deve ultrapassar 60 caracteres.");
		}
		// Senha
		if (usuario.getSenha() == null || !isValidMinimunLength(usuario.getSenha(), 4))
			erros.put("senha", "A senha deve ter pelo menos 4 caracteres.");
		if (!erros.isEmpty())
			throw new UsuarioException(mensagemDeErro, erros);
	}

}
