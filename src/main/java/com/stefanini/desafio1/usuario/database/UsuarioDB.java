package com.stefanini.desafio1.usuario.database;

import org.springframework.data.repository.CrudRepository;

import com.stefanini.desafio1.usuario.Usuario;

/**
 * @author Brunno José Guimarães de Almeida.
 * @since 20 de agosto de 2020.
 */
public interface UsuarioDB extends CrudRepository<Usuario, Integer> {
	
	/**
	 * Realiza a autenticação do usuário através de senha e email fornecidos.
	 * @param email Endereço de Email.
	 * @param senha Senha.
	 * @return Usuário caso exista.
	 */
	public Usuario autenticar(String email, String senha);
	
	/**
	 * Retorna o usuário com o email indicado.
	 * @param email E-mail.
	 * @return Usuário com o e-mail indicado.
	 */
	public Usuario getByEmail(String email);

}
