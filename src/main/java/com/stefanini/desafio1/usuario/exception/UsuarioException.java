package com.stefanini.desafio1.usuario.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * <b>UsuarioException</b><br>
 * Exceção para a entidade Usuario.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 07 de agosto de 2020.
 */
public class UsuarioException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public String mensagem;

	private Map<String, String> erros;

	/**
	 * Construtor.
	 * 
	 * @param excecao Mensagem da exceção.
	 */
	public UsuarioException(String excecao) {
		super(excecao);
		this.mensagem = excecao;
		erros = new HashMap<String, String>();
	}

	/**
	 * Construtor.
	 * 
	 * @param excecao Mensagem da exceção.
	 * @param erros   Mapa Contendo os erros.
	 */
	public UsuarioException(String excecao, Map<String, String> erros) {
		super(excecao);
		this.erros = erros;
	}

	/**
	 * Adiciona o erro ao campo.
	 * 
	 * @param campo     Nome do campo.
	 * @param descricao Descrição do erro.
	 */
	public void addErro(String campo, String descricao) {
		erros.put(campo, descricao);
	}

	/**
	 * Retorna a mensagem de erro.
	 * 
	 * @return Mensagem de erro.
	 */
	public String getMensagem() {
		return mensagem;
	}

	/**
	 * Define a mensagem de erro.
	 * 
	 * @param mensagem Mensagem de erro.
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	/**
	 * Retorna um mapa contendo os erros.
	 * 
	 * @return Mapa contendo os erros.
	 */
	public Map<String, String> getErros() {
		return erros;
	}

	/**
	 * Define um mapa contendo os erros.
	 * 
	 * @param erros Mapa contendo os erros.
	 */
	public void setErros(Map<String, String> erros) {
		this.erros = erros;
	}

}
