package com.stefanini.desafio1.usuario.service;

import java.sql.Timestamp;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stefanini.desafio1.tarefa.ListaDeTarefa;
import com.stefanini.desafio1.tarefa.Tarefa;
import com.stefanini.desafio1.tarefa.database.ListaDeTarefaDB;
import com.stefanini.desafio1.tarefa.database.TarefaDB;
import com.stefanini.desafio1.usuario.Usuario;
import com.stefanini.desafio1.usuario.database.UsuarioDB;
import com.stefanini.desafio1.usuario.exception.UsuarioException;
import com.stefanini.desafio1.usuario.util.UsuarioValidator;
import com.stefanini.desafio1.util.MD5Util;

/**
 * <b>UsuarioService</b><br>
 * Implementação da classe de serviço para a entidade Usuario.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Service
public class UsuarioService {

	@Autowired
	private UsuarioDB database;

	@Autowired
	private ListaDeTarefaDB listaDeTarefasDB;

	@Autowired
	private TarefaDB tarefaDB;

	/**
	 * Realiza a autenticação do usuário através do email e senha, retornando um
	 * Usuário caso estes estejam corretos.
	 * 
	 * @param email Endereço de email.
	 * @param senha Senha.
	 * @return Usuário.
	 * @throws UsuarioException Exceção de usuário
	 */
	public Usuario autenticar(String email, String senha) throws UsuarioException {
		if (email != null)
			email = email.toLowerCase();
		Usuario usuario = database.autenticar(email, MD5Util.toMd5(senha));
		if (usuario == null)
			throw new UsuarioException("Email ou senha inválidos.");
		return usuario;
	}

	/**
	 * Realiza o registro do usuário indicado.
	 * 
	 * @param nome  Nome do usuário.
	 * @param email E-mail.
	 * @param senha Senha.
	 */
	public void registrar(String nome, String email, String senha) {
		// Remoção de espaços em branco.
		if (nome != null)
			nome = nome.trim();
		if (email != null)
			email = email.trim().toLowerCase();
		// Verificação de usuario já existir com email informado.
		if (email != null) {
			Usuario usuarioExistente = database.getByEmail(email);
			if (usuarioExistente != null) {
				UsuarioException ue = new UsuarioException("O e-mail informado não está disponível para registro.");
				ue.addErro("email", "E-mail já existe no cadastro.");
				throw ue;
			}
		}
		// Criação de novo usuário.
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setEmail(email);
		usuario.setSenha(senha);
		usuario.setDataDeCadastro(new Timestamp(System.currentTimeMillis()));
		// Dispara um UsuarioException caso o usuário não passe na validação.
		UsuarioValidator.validate(usuario, "Não foi possível registrar usuário.");
		usuario.setSenha(MD5Util.toMd5(senha));
		database.save(usuario);
		// Criação de lista padrão.
		ListaDeTarefa lista = new ListaDeTarefa();
		lista.setDataDeCadastro(new Timestamp(System.currentTimeMillis()));
		lista.setUsuario(usuario);
		lista.setNome("Tarefas");
		listaDeTarefasDB.save(lista);
	}

	/**
	 * Realiza o encerramento da conta do Usuário informado, removendo todos os
	 * itens que pertençam ao usuário.
	 * 
	 * @param usuario Usuário.
	 */
	@Transactional
	public void encerrarConta(Usuario usuario) {
		if (usuario == null)
			throw new UsuarioException("Não foi possível encerrar conta, o usuário não está logado.");
		List<ListaDeTarefa> retorno = listaDeTarefasDB.getList(usuario);
		for (ListaDeTarefa lista : retorno) {
			List<Tarefa> tarefas = tarefaDB.getList(lista);
			for (Tarefa tarefa : tarefas)
				tarefaDB.delete(tarefa);
			listaDeTarefasDB.delete(lista);
		}
		database.delete(usuario);
	}
}
