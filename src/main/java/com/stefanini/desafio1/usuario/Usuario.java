package com.stefanini.desafio1.usuario;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * <b>Usuario</b><br>
 * Entidade que representa um Usuário.
 * 
 * @author Brunno José Guimarães de Almeida.
 * @since 06 de agosto de 2020.
 */
@Entity
@NamedQuery(name = "Usuario.autenticar", query = "SELECT u FROM Usuario u WHERE u.email = ?1 AND u.senha = ?2")
@NamedQuery(name = "Usuario.getByEmail", query = "SELECT u FROM Usuario u WHERE u.email = ?1")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private Timestamp dataDeCadastro;

	@Column(length = 80)
	private String nome;

	@Column(length = 60)
	private String email;

	@Column(length = 32)
	@JsonIgnore
	private String senha;

	/**
	 * Retorna o ID do usuário.
	 * 
	 * @return ID do usuário.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Define o ID do usuário.
	 * 
	 * @param id ID do usuário.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retorna o nome do usuário.
	 * 
	 * @return Nome do usuário.
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Define o nome do usuário.
	 * 
	 * @param nome Nome do usuário.
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Retorna o endereço de email.
	 * 
	 * @return Endereço de e-mail.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Define o endereço de email.
	 * 
	 * @param email Endereço de email.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Retorna a senha do usuário.
	 * 
	 * @return Senha do usuário.
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Define a senha do usuário.
	 * 
	 * @param senha Senha do usuário.
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Retorna a data de cadastro.
	 * 
	 * @return Data de cadastro.
	 */
	public Timestamp getDataDeCadastro() {
		return dataDeCadastro;
	}

	/**
	 * Define a data de cadastro.
	 * 
	 * @param dataDeCadastro Data de cadastro.
	 */
	public void setDataDeCadastro(Timestamp dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

}