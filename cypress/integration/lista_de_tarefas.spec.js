describe('ListaController', () => {
	it('Cadastro do Usuário para Execução dos testes.', () => {
		// Tenta autenticar usuário que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Realiza o registro do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Verifica se no usuário cadastrado foi criada a Lista de Tarefas padrão.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})

	})
	it('Impedir carregar lista de tarefas sem estar logado.', () => {
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível carregar lista de tarefas, o usuário não está logado.')
		})
	})
	it('Impedir adicionar lista de tarefas sem estar logado.', () => {
		cy.request({
			method: 'POST',
			url: '/lista/adicionar',
			form: true,
			body: {
				nome: 'Lista 2',
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível gravar a lista, o usuário não está logado.')
		})
	})
	it('Impedir adicionar lista de tarefas com nome vazio.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas do usuário.		
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
		// Tenta adicionar uma nova lista de nome vazio
		cy.request({
			method: 'POST',
			url: '/lista/adicionar',
			form: true,
			body: {
				nome: '',
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível gravar lista.')
			expect(response.body.nome).to.eq('O nome da lista não pode ser vazio.')
		})
		// Verifica se as listas de tarefas do usuário permanecem inalteradas.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
	})
	it('Impedir adicionar lista de tarefas com nome maior que 50 caracteres', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
		// Tenta realizar a inserção de uma lista com o nome com mais de 50 caracteres.
		cy.request({
			method: 'POST',
			url: '/lista/adicionar',
			form: true,
			body: {
				nome: 'Lista 1234567890123465789012345679801234567890123456790',
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.nome).to.eq('O nome da lista não pode exceder 50 caracteres.')
			expect(response.body.mensagem).to.eq('Não foi possível gravar lista.')
		})
		// Verifica se não houve alteração nas listas do usuários.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
	})
	it('Adicionar lista de tarefas válida.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
		cy.request({
			method: 'POST',
			url: '/lista/adicionar',
			form: true,
			body: {
				nome: 'Lista 02',
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body.nome).to.eq('Lista 02')
		})
		// Verifica se houve adição da lista.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[1].nome).to.eq('Lista 02')
			expect(response.body.length).to.eq(2)
		})
	})
	it('Excluir lista de tarefas.', () => {
		// Realiza a autenticação do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[1].nome).to.eq('Lista 02')
			expect(response.body.length).to.eq(2)

			// Realiza a exclusão da segunda lista
			cy.request({
				url: '/lista/excluir/' + response.body[1].id,
				failOnStatusCode: true
			})
		})
		// Verifica se a lista de tarefa foi excluída.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})

	})
	it('Impedir Excluir lista de tarefas sem logar', () => {
		// Realiza a autenticação do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)

			// Efetuar Logout do usuário
			cy.request({
				url: '/logout',
				failOnStatusCode: true
			})

			// Realiza a exclusão da lista
			cy.request({
				url: '/lista/excluir/' + response.body[0].id,
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(400)
				expect(response2.body.mensagem).to.eq('Não foi possível excluir lista, o usuário não está logado.')
			})
		})
		// Realiza a autenticação do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica se a lista de tarefa foi excluída.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})

	})
	it('Impedir excluir lista de tarefas inexistente', () => {
		// Realiza a autenticação do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)

		})
		// Realiza a exclusão da lista de tarefas que não existe.
		cy.request({
			url: '/lista/excluir/-1',
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível excluir, a lista não existe.')
		})
		// Verifica se as listas de tarefas se mantiveram.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
	})
	it('Excluir lista de tarefas que possui tarefas cadastradas', () => {
		// Realiza a autenticação do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Verifica as listas de tarefas já existentes.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)

			// Adição da Tarefa 01.
			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: '' + response.body[0].id + '',
					descricao: 'Tarefa 01'
				},
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
			})
			// Adição da Tarefa 02.
			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: '' + response.body[0].id + '',
					descricao: 'Tarefa 02'
				},
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
			})

			// Realiza a exclusão da lista de tarefas com as tarefas.
			cy.request({
				url: '/lista/excluir/'+response.body[0].id,
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(200)
			})
		})

		// Verifica se a lista de tarefa foi exluída.
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body.length).to.eq(0)
		})
	})
	it('Encerrar a conta de usuários usados no teste.', () => {
		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
})