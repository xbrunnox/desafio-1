describe('IndexController', () => {
	it('Cadastro do Usuário para Execução dos testes.', () => {
		// Tenta autenticar usuário que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Tenta autenticar usuário 2 que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Realiza o registro do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o registro do segundo usuario.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'User Lastname',
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})

	it('Realiza o Logout do usuário', () => {
		// Realiza a autenticação com o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o logout do usuário
		cy.request({
			url: '/logout',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	});
	
	it('Realiza o RESET na conta do usuário', () => {
		// Realiza a autenticação com o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body.length).to.eq(1)
		})
		// Efetua o reset na conta do usuário
		cy.request({
			url: '/reset',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body.length).to.eq(0)
		})
	});
	
	it('Impede o RESET se o usuário não estiver logado', () => {
		// Efetua o reset na conta do usuário
		cy.request({
			url: '/reset',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	});

	it('Encerrar a conta de usuários usados no teste.', () => {
		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})

		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
})