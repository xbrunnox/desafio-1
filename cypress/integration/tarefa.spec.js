describe('TarefaController', () => {
	it('Cadastro do Usuário para Execução dos testes.', () => {
		// Tenta autenticar usuário que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Tenta autenticar usuário 2 que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Realiza o registro do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o registro do segundo usuario.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'User Lastname',
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Adiciona tarefa à lista padrão do Usuário.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			// Tenta adicionar a tarefa na lista padrão
			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: response.body[0].id,
					descricao: 'Tarefa 01',
				},
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
				expect(response2.body.descricao).to.eq('Tarefa 01')
			})
		})
	})
	it('Impede adicionar tarefa com descrição vazia.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			// Tenta adicionar a tarefa na lista padrão
			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: response.body[0].id,
					descricao: '',
				},
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(400)
				expect(response2.body.mensagem).to.eq('Não foi possível gravar tarefa.')
				expect(response2.body.descricao).to.eq('A descrição da tarefa não pode ser vazia.')
			})
		})
	})
	it('Impede adicionar tarefa com descrição com mais de 200 caracteres.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')

			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: response.body[0].id,
					descricao: '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123',
				},
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(400)
				expect(response2.body.mensagem).to.eq('Não foi possível gravar tarefa.')
				expect(response2.body.descricao).to.eq('O tamanho máximo da descrição da tarefa é 512 caracteres.')
			})
		})
	})

	it('Impede adicionar tarefa na lista de outro usuário.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')

			// Autentica o segundo usuário
			cy.request({
				method: 'POST',
				url: '/usuario/autenticar',
				form: true,
				body: {
					email: 'user@gmail.com',
					senha: '4321'
				},
				failOnStatusCode: true
			}).then((response3) => {
				expect(response3.status).to.eq(200)
			})

			// Tenta com o segundo usuario adicionar a tarefa na lista do primeiro usuário 
			cy.request({
				method: 'POST',
				url: '/tarefa/adicionar',
				form: true,
				body: {
					idLista: response.body[0].id,
					descricao: 'Tarefa 02',
				},
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(400)
				expect(response2.body.mensagem).to.eq('Não foi possível gravar, a lista não existe.')
			})
		})
	})

	it('Recupera a tarefa com o ID indicado.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas[0].descricao).to.eq('Tarefa 01')
			// Recupera a tarefa com o ID indicado.
			cy.request({
				url: '/tarefa/get/' + response.body[0].tarefas[0].id,
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
				expect(response2.body.descricao).to.eq('Tarefa 01')
			})
		})
	})

	it('Impede a recuperação de uma tarefa cujo o ID não existe.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a tarefa com o ID indicado.
		cy.request({
			url: '/tarefa/get/' + '-1',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body).to.eq('')
		})
	})

	it('Impede a recuperação de uma tarefa a partir de outro usuário.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas[0].descricao).to.eq('Tarefa 01')

			// Autentica o segundo usuário.
			cy.request({
				method: 'POST',
				url: '/usuario/autenticar',
				form: true,
				body: {
					email: 'user@gmail.com',
					senha: '4321'
				},
				failOnStatusCode: true
			}).then((response3) => {
				expect(response3.status).to.eq(200)
			})

			// Recupera a tarefa com o ID indicado.
			cy.request({
				url: '/tarefa/get/' + response.body[0].tarefas[0].id,
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
				expect(response2.body).to.eq('')
			})
		})
	})

	it('Impede a exclusão de uma tarefa a partir de outro usuário.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas[0].descricao).to.eq('Tarefa 01')
			expect(response.body[0].tarefas.length).to.eq(1)

			// Autentica o segundo usuário.
			cy.request({
				method: 'POST',
				url: '/usuario/autenticar',
				form: true,
				body: {
					email: 'user@gmail.com',
					senha: '4321'
				},
				failOnStatusCode: true
			}).then((response3) => {
				expect(response3.status).to.eq(200)
			})

			// Recupera a tarefa com o ID indicado.
			cy.request({
				url: '/tarefa/excluir/' + response.body[0].tarefas[0].id,
				failOnStatusCode: false
			}).then((response2) => {
				expect(response2.status).to.eq(400)
				expect(response2.body.mensagem).to.eq('A tarefa não existe, ou já se encontra excluída.')
			})
		})
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas[0].descricao).to.eq('Tarefa 01')
			expect(response.body[0].tarefas.length).to.eq(1)
		})
	})
	
	it('Realiza a exclusão de uma tarefa.', () => {
		// Autentica o usuário
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas[0].descricao).to.eq('Tarefa 01')
			expect(response.body[0].tarefas.length).to.eq(1)

			// Exclui a tarefa com o ID indicado.
			cy.request({
				url: '/tarefa/excluir/' + response.body[0].tarefas[0].id,
				failOnStatusCode: true
			}).then((response2) => {
				expect(response2.status).to.eq(200)
				expect(response2.body).to.eq('ok')
			})
		})
		// Recupera a lista de tarefas do usuário
		cy.request({
			url: '/lista/listasDeTarefas',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
			expect(response.body[0].nome).to.eq('Tarefas')
			expect(response.body[0].tarefas.length).to.eq(0)
		})
	})

	it('Encerrar a conta de usuários usados no teste.', () => {
		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})

		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'user@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
})