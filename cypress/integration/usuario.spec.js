describe('Index deslogado, redirect to /login', () => {
	it('Verifica redirect do index', () => {
		cy.visit('/')
		cy.url().should('include', '/login')
		cy.contains('Login')
		cy.contains('Email')
		cy.contains('Entrar')
		cy.contains('Registre-se')
	})
	it('Clica em registrar.', () => {
		cy.url().should('include', '/login')
		cy.contains('Registre-se').click()
		cy.contains('Nome')
		cy.contains('Email')
		cy.contains('Senha')
		cy.contains('Senha Check')
		cy.contains('inv')
	})
})
describe('UsuarioController', () => {
	it('Autenticar sem cadastro', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
	it('Cadastro com nome em branco', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: '',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.nome).to.eq('O nome do usuario não pode ser vazio.')
		})
	})
	it('Cadastro com nome com menos de 10 caracteres', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.nome).to.eq('O nome do usuario deve ter mais de 10 caracteres.')
		})
	})
	it('Cadastro com nome com 81 caracteres, mais de 80 caracteres', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno 12345678901234567890123456789012345679801234567890123456790123456890123456',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.nome).to.eq('O nome do usuario não deve ter mais que 80 caracteres.')
		})
	})
	it('Cadastro com email inválido', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com.',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.email).to.eq('Endereço de e-mail inválido.')
		})
	})
	it('Cadastro com email vazio', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: '',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.email).to.eq('Endereço de e-mail inválido.')
		})
	})
	it('Cadastro com email com 61 caracteres, mais de 60 caracteres', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp123456012312345698712365498712346790123456789012@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.email).to.eq('Endereço de e-mail não deve ultrapassar 60 caracteres.')
		})
	})
	it('Cadastro com senha vazia.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: ''
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.senha).to.eq('A senha deve ter pelo menos 4 caracteres.')
		})
	})
	it('Cadastro com senha curta, 3 caracteres.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '123'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.senha).to.eq('A senha deve ter pelo menos 4 caracteres.')
		})
	})
	it('Cadastro com todos os campos vazios.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: '',
				email: '',
				senha: ''
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.senha).to.eq('A senha deve ter pelo menos 4 caracteres.')
			expect(response.body.email).to.eq('Endereço de e-mail inválido.')
			expect(response.body.nome).to.eq('O nome do usuario não pode ser vazio.')
		})
	})
	it('Cadastro com nome e senha curtos.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno',
				email: 'temp@gmail.com',
				senha: '123'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível registrar usuário.')
			expect(response.body.senha).to.eq('A senha deve ter pelo menos 4 caracteres.')
			expect(response.body.nome).to.eq('O nome do usuario deve ter mais de 10 caracteres.')
		})
	})
	it('Cadastro com dados válidos.', () => {
		// Tenta autenticar usuário que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Realiza o registro do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Cadastro de usuário com dados válidos e email já existente.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '4321'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('O e-mail informado não está disponível para registro.')
			expect(response.body.email).to.eq('E-mail já existe no cadastro.')
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Autenticar usuário válido.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Autenticar usuário válido com e-mail com letras maiusculas.', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'tEMp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Autenticar usuário válido, com senha incorreta', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '123'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
	it('Autenticar usuário válido, com senha vazia', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: ''
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
	it('Autenticar usuário vazio, com senha válida', () => {
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: '',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
	it('Encerrar conta de usuário não logado.', () => {
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Não foi possível encerrar conta, o usuário não está logado.')
		})
		// Realiza a autenticação para checar que a conta não foi encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Encerrar conta de usuário válido.', () => {
		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
	it('Recadastro de usuário com conta encerrada.', () => {
		// Tenta autenticar usuário que não existe.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
		// Realiza o registro do usuário.
		cy.request({
			method: 'POST',
			url: '/usuario/registrar',
			form: true,
			body: {
				nome: 'Brunno Almeida',
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza a autenticação com o usuário que acabou de ser registrado.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
	it('Encerrar a conta de usuários usados no teste.', () => {
		// Realiza a autenticação para realizar o encerramento da conta.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar',
			form: true,
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Realiza o encerramento da conta.
		cy.request({
			url: '/usuario/encerrarConta',
			failOnStatusCode: true
		}).then((response) => {
			expect(response.status).to.eq(200)
		})
		// Tenta autenticar usuário que teve a conta encerrada.
		cy.request({
			method: 'POST',
			url: '/usuario/autenticar', // baseUrl is prepended to url
			form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
			body: {
				email: 'temp@gmail.com',
				senha: '1234'
			},
			failOnStatusCode: false
		}).then((response) => {
			expect(response.status).to.eq(400)
			expect(response.body.mensagem).to.eq('Email ou senha inválidos.')
		})
	})
})